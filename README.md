# Back End PerMM Project

This Project is the back end of Lomize Group's PerMM database. You can find the front end PerMM code [here](https://bitbucket.org/lomize-group/lomize-permm-frontend). This project listens for requests from the front end and returns JSON formatted data per the request. This is a [Ruby on Rails](https://rubyonrails.org/) application that uses [PostgreSQL](https://www.postgresql.org/) as the database. The application is deployed on [Heroku](https://www.heroku.com/about) under the account lomize.group.x@gmail.com.

Below you will find some information on how to run the code, perform common tasks, and where various data must be located.

## Prerequisites
- Install the [Ruby programming language](https://www.ruby-lang.org/en/) version 2.7.2
  using [Ruby Version Manager (RVM)](https://rvm.io)
- Install the [Postgres](https://www.postgresql.org/) database
    * If you are using Mac OS X, the easiest method is to download [Postgres.app](http://postgresapp.com/)
    * Or you can install postgres with [Homebrew](https://brew.sh/):
      ```
      brew install postgresql
      postgres -D /usr/local/var/postgres &
      psql postgres
      ```
- Install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)
    * Log in with `heroku login`

## Running the code
Make sure your computer is running in a linux based env. MacOSX and Linux are great, if you are using windows you may need to boot inside a linux partition. You will need  ruby and rails. Additionally you will need to have a postgres server running locally for this project to run.

### Get the code
First clone the code repository to your local environment:
```
  mkdir permm-back-end
  cd permm-back-end
  git clone git@bitbucket.org:lomize-group/lomize-permm-backend.git
  cd lomize-permm-backend
  git checkout master
```

### External dependencies
All the necessary dependencies for the Ruby application are specified in the `Gemfile`. In the project directory, run the following command to install these dependencies. This may take a while the first time.
```
  bundle install
```

### Running the code
Start the Rails application in your local environment by running the following command:
```
  rails server
```
This command will listen for requests on localhost:3000. To listen on a different port, you can add the `-p` flag:
```
  rails server -p <port_number>
```
 Rails will automatically hot-load any changes you make to the project. Additionally, Rails will give output in your terminal if any runtime or build errors occur.

(There are `bundle` and `rails` executables provided under `bin/` but the globally installed versions will work.)

## Additional Commands
There are many useful scripts specified in the `lib/tasks/*.rake` files. These are separated into files and namespaces based on what they do. For example, commands related to deployment will be found in the `deploy.rake` file under the namespace `deploy`.

#### Data-related commands

At this point it is probably helpful to note that there a lot of helpful one-off commands that are inside /lib/tasks. The name scheme is always namespace:task.

```
rake data:renumber
```
This renumbers the ordering property of various objects to be floats starting at 1.0. For example if we have three objects at 1.0, 1.2, and 4.7 running this command would produce 1.0, 2.0, 3.0

```
rake data:update_hierarchy
```
This rebuilds the global hierarchy for objects. For example if a class changes type, then all the objects below class (superfamily, family... etc) must be renumbered and reordered to preserve consistant ordering across large lists.

```
rake data:recache
	# this runs: 
	rails data:update_hierarchy
	rails data:renumber

```

```
rake data:pull
```
This copies the production database to your local environment. It completely overwrites your local copy, including both the schema and the data.

```
psql -d lomize-permm_development -c "<sql command>"
```

This can be used to manipulate your local database without using the server.

#### Deployment commands

```
rake deploy:code
```
This command only deploys new code and restarts the server running on Heroku. It does not make database changes. This command is very fast and should be used when only code is changed.

```
# WARNING: will overwrite production database with your local copy
rake deploy:from_local
```
This will deploy code changes and will also replace the production database with your local copy. This is often useful when importing large amounts of data locally.

#### Data import scripts
Data import and cleaning scripts are specified in `import.rake` under the `import` namespace. Many of these are one-time use for importing data from a specific format, but they are saved in case they need to be reused.


## Deploying Changes
After changes have been made, you should commit and push your changes to the branch "master" of the main repository. You can check your current branch with the following command:
```
  git branch
```
You can commit and push changes with the following commands:
```
  git add .
  git commit -m "My helpful message about the changes I've made"
  git push
```

Once changes have been pushed to master, you are ready to deploy to Heroku. Before deploying to Heroku, make sure you are logged in to the correct account. For updating the PerMM back end, this account should be lomize.group.x@gmail.com.

``` 
heroku login
heroku git:remote -a lomize-group-permm
```

This project is configured for one-line deployment. There are a few different types of deployment.

### Deploying only code chaneges
When you want to deploy code changes, run the following command:

```
  rake deploy:code
```
This runs a code deployment and usually takes a couple of minutes to complete.

### Deploying code and database changes
To deploy all code and database changes, run the following command:
```
  rake deploy:from_local
```
This will deploy code changes and will also replace the production database with your local copy. This is often useful when importing large amounts of data locally. Because the production databse is replaced, you should copy all of its data to your local environment before making and deploying database changes. You can do this with the following command:
```
  rake data:pull
```


**Example of a simple change**

If you wanted to change the attributes sent to the front end on a request for a primary_structure object, you would make some changes to the PrimaryStructureSerializer and then make sure the code works as intended.

Then you would run the following commands:
```
  git add .
  git commit -m "Added existing attributes to primary_structure response"
  git push
  rake deploy:code
```

## How this Project is Organized
This project is setup like many classic react.js projects that make use of webpack. 
```
/app
	/controllers
	/models
	/serializers
/bin
/config
	routes.rb
	....
/db
	/migrate
	schema.rb
	seeds.rb
	seed_file.json
/lib
  /tasks
  	data.rake
  	deploy.rake
....
Gemfile
```

There are several important locations:
### /config/routes.rb
This is where all the routes that this projects responds to are defined. If you want to add another route, you must do it here. 

ex.  match "families/:id" => 'families#find_one', :via => :get

This route indicates that /families/1 would call the function 'find one' inside families_controller

### /app/controllers
This is where the application controllers are defined. This is where the methods called by the routes are defined and called. Every controller is a subclass of application_controller and that controller has various helpful methods that every controller uses (i.e. parameter parsing).


### /app/models
This is where the object models are defined in ruby. This is where ruby will map postgres objects to ruby models. Also you can configure various active_record options here.

### /app/serializers
This is the 'view' layer and allows you to easily control the end json view of any object or page without having to touch the models. If you must change the json output of an endpoint you should probably do it here.


### /config/migrate/
This is where we keep track and edit the params on an object. Attempt not to add unneeded migrations. If you add or remove attributes  you should run running ```rake data:pull``` first, as it will copy the database from production to your local environment. To deploy database changes to Heroku you will likely need to run ```rake deploy:from_local```.

### /lib/tasks/
This is where we need tasks. Programmers are lazy and don't like remembering a lot of convoluted commands needed to do common tasks. We make tasks instead!

# Conventions
These are our standards for how we name and code things.

## Git
We use the gitflow branching method.

- **/master** is used for the current deployed stable version. If code is on master it is always working and may always be deployed.
- **/feat/about_page** the feat/x convention is used to denote features that developers are working on. These may be broken at any time. The idea is to merge master onto a feat branch, fix changes and then merge back onto master. Then deploy changes that way.
- **Branch_names** are always in underspaced. 
  - ```master``` is the master deployed branch
  - ```feat/x``` is a branch that someone is working on
  - ```pom/x``` is a proof of model that is being used as a dry-run for an experimental technology. This is when we want to use a new technology but we aren't sure what it will break.
- **folder_names** and **Ruby file_names** are always in underspaced. 

## Objects and Style
Objects are always in CamelCase such as: FamiliesController. All other names should be properly pluralized (if needed) and always denoted_in_space_case


# Security
Although all data in the PerMM database is public and there is no need to store sensitive data, security is still a concern. For example, and SQL injection attack could be used to destroy the database, requiring manual backup restores. The following are some guidelines to follow to prevent or mitigate attacks and malicious use of PerMM.

## User Input and SQL Injection
The ActiveRecord framework used in PerMM for accessing objects from the database does not fully protect against SQL injection. See the [Ruby on Rails SQL injection information](https://rails-sqli.org/) for more information on specific cases in which ActiveRecord is vulnerable to SQL injection.

### Defending against SQL Injection
If you are unfamiliar with SQL injection, it is very important that you educate yourself on the issue before deploying changes. A good resource for this is the [OWASP artice on SQL injection](https://www.owasp.org/index.php/SQL_Injection).

#### Be careful: passing user input directly to ActiveRecord
The Rails SQL injection guidelines show that sometimes parameters are sanitized. For example:
```
  # params[:pdbid] is a string
  proteins = Protein.where(pdbid => params[:pdbid])
```
Rails uses a prepared SQL query for this, which eliminates the risk of SQL injection in this specific example.

However, passing raw SQL strings to an ActiveRecord query *is* vulnerable to SQL injection:
```
  # params[:pdbid] is a string
  proteins = Protein.where("pdbid = #{params[:pdbid]}")
```
Do NOT do this. If a raw SQL string must be passed to a method such as `ActiveRecord::where`, the user input *must* be sanitized first. See the [Rails sanitization API](https://api.rubyonrails.org/classes/ActiveRecord/Sanitization/ClassMethods.html) for details.

#### Making *non-string* user inputs safe
The main issue with SQL injection comes from user input values that are used to query and search the database. For all non-string values, this can easily be taken care of by casting the input to its correct type before using it.
```
@page_size = 50
if params[:page_size]
  page_size = params[:page_size].to_i
end
```
This code will convert the page_size user input to an integer and store it in the controller's member variable "@page_size". If an error is thrown because the value cannot be casted to an integer, Rails will return a "500 internal server error" to the requester. This exception does not need to be handled because it means someone attempted to misuse the page_size parameter; it is okay if we do not give a good response to their request.

Generally, user inputs will be stored as global variables with a `:before_action` callback. See `ApplicationController::parse_paging_params` in `/app/controllers/application_controller.rb`; this is used as the `:before_action` callback in most controllers. All common user inputs should be placed here.


#### Making *string* user inputs safe
As mentioned above, you should sanitize strings with the [Rails sanitization methods](https://api.rubyonrails.org/classes/ActiveRecord/Sanitization/ClassMethods.html), or **pass strings as hash parameters**:
```
  # params[:pdbid] is a string
  proteins = Protein.where(pdbid => params[:pdbid])
  families = ProteinFamily.order(@sort_column => @sort_direction)
```
NOT as SQL strings, unless you have sanitized them and you absolutely have to:
```
  # params[:pdbid] is a string
  proteins = Protein.where("pdbid = #{params[:pdbid]}")
  families = ProteinFamily.order("#{@sort_column} #{@sort_direction}")
```


# Known Working Version
This should be easy to upgrade, known stable versions are:
```
ruby 2.3.1p112
postgres (PostgreSQL) 10.1
```
