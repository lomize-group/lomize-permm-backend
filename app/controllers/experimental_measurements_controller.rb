class ExperimentalMeasurementsController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	

	# match "experimental_measurements" => 'experimental_measurements#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ExperimentalMeasurement.all.order(:id)
			file = file_response(ExperimentalMeasurement.column_names, objects)
			return send_data file, :filename => "experimental_measurements-#{Date.today}.csv", :type => @fileFormat
		end
		
		# primary order by sort column
		if ExperimentalMeasurement.type_for_attribute(@sort_column).type == :string
			objects = ExperimentalMeasurement.order("lower(#{@sort_column}) #{@sort_direction}")
		else
			objects = ExperimentalMeasurement.order(@sort_column => @sort_direction)
		end

		# secondary order by id
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		# finally search/paginate
		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		count = ExperimentalMeasurement.search(@search).count
		result = paged_response(objects, count, "Experimental Measurements")

		render :json =>result, :status => 200, serializer: ExperimentalMeasurementsPageSerializer
	end

	# match "experimental_measurements/:id" => 'experimental_measurements#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ExperimentalMeasurement.find(@id)
		render :json =>result, :status => 200, serializer: ExperimentalMeasurementsSerializer,
		include: [
			'molecule',
			'molecule.class_type',
			'molecule.group'
		]
	end

end