class StatsController < ApplicationController
	require 'ostruct'

	# match "stats" => 'stats#basic_stats', :via => :get
	def basic_stats
		response.headers['Access-Control-Allow-Origin'] = '*'
		stats = OpenStruct.new(
			:groups => Group.all.count,
			:class_types => ClassType.all.count,
			:membrane_systems => MembraneSystem.all.count,
			:molecules => Molecule.count
			)
		render :json =>stats, :status => 200
	end

end