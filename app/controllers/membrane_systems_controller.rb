class MembraneSystemsController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	

	# match "membrane_systems" => 'membrane_systems#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = MembraneSystem.all.order(:id)
			file = file_response(MembraneSystem.column_names, objects)
			return send_data file, :filename => "membrane_systems-#{Date.today}.csv", :type => @fileFormat
		end
		objects = MembraneSystem.paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, MembraneSystem.all.count, "Membrane Systems")
		render :json =>result, :status => 200, serializer: MembraneSystemsPageSerializer
	end

	# match "membrane_systems/:id" => 'membrane_systems#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = MembraneSystem.find(@id)
		render :json =>result, :status => 200, serializer: MembraneSystemsSerializer
	end

	# match "membrane_systems/:id/experimental_measurements" => 'membrane_systems#find_experimental_measurements_of', :via => :get
	def find_experimental_measurements_of
		response.headers['Access-Control-Allow-Origin'] = '*'

		objects = ExperimentalMeasurement.where(:membrane_system_id => @id)

		# primary order by sort column
		if ExperimentalMeasurement.type_for_attribute(@sort_column).type == :string
			objects = objects.order("lower(#{@sort_column}) #{@sort_direction}")
		else
			objects = objects.order(@sort_column => @sort_direction)
		end

		# secondary order by id
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		membraneName = MembraneSystem.find(@id).name
		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		count = ExperimentalMeasurement.where(:membrane_system_id => @id).search(@search).count

		result = paged_response(objects, count, membraneName)
		render :json =>result, :status => 200, serializer: ExperimentalMeasurementsPageSerializer
	end
	
end