class ClasstypesController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	

	# match "classtypes" => 'classtypes#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = ClassType.all.order(:id)
			file = file_response(ClassType.column_names, objects)
			return send_data file, :filename => "classtypes-#{Date.today}.csv", :type => @fileFormat
		end
		objects = ClassType.paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, ClassType.all.count, "Classes")
		render :json =>result, :status => 200, serializer: ClasstypesPageSerializer
	end

	# match "classtypes/:id" => 'classtypes#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = ClassType.find(@id)
		render :json =>result, :status => 200, serializer: ClasstypesSerializer
	end

	# match "classtypes/:id/molecules" => 'classtypes#find_molecules_of', :via => :get
	def find_molecules_of
		response.headers['Access-Control-Allow-Origin'] = '*'
		classtypeName = ClassType.find(@id).name
		isSortByMembraneSystemVal = @sort_column.start_with? 'membrane_system_id='

		if isSortByMembraneSystemVal
			searchMembraneSystemId = @sort_column.split('=')[-1]
			# in this version I got an error that sanitize is not a method in active records
			# objects = Molecule.where(:class_type_id => @id).order("log_perm_coeff #{@sort_direction} NULLS LAST")
			# .joins("LEFT JOIN experimental_measurements ON experimental_measurements.molecule_id = molecules.id AND experimental_measurements.membrane_system_id = " + ActiveRecord::Base.sanitize(searchMembraneSystemId))
			# .search(@search).paginate(:page => @page, :per_page => @per_page)
			objects = Molecule.where(:class_type_id => @id).order("log_perm_coeff #{@sort_direction} NULLS LAST")
			.joins("LEFT JOIN experimental_measurements ON experimental_measurements.molecule_id = molecules.id AND experimental_measurements.membrane_system_id = " + ActiveRecord::Base.sanitize_sql_array(["?", searchMembraneSystemId]))
			.search(@search).paginate(:page => @page, :per_page => @per_page)
		else
			objects = Molecule.where(:class_type_id => @id)

			# primary order by sort column
			if Molecule.type_for_attribute(@sort_column).type == :string
				objects = objects.order("lower(#{@sort_column}) #{@sort_direction}")
			else
				objects = objects.order(@sort_column => @sort_direction)
			end

			# secondary order by id
			if @sort_column != 'id'
				objects = objects.order(:id => @sort_direction)
			end

			# finally search/paginate
			objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		end
		count = Molecule.where(:class_type_id => @id).search(@search).count
		result = paged_response(objects, count, classtypeName)
		render :json =>result, :status => 200, serializer: MoleculesPageSerializer
	end
	
end