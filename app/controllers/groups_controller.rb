class GroupsController < ApplicationController
	# declared in the ApplicationController
	before_action :parse_paging_params
	

	# match "groups" => 'groups#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Group.all.order(:id)
			file = file_response(Group.column_names, objects)
			return send_data file, :filename => "groups-#{Date.today}.csv", :type => @fileFormat
		end
		objects = Group.paginate(:page => @page, :per_page => @per_page).order(@sort_column => @sort_direction)
		result = paged_response(objects, Group.all.count, "Groups")
		render :json =>result, :status => 200, serializer: GroupsPageSerializer
	end

	# match "groups/:id" => 'groups#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Group.find(@id)
		render :json =>result, :status => 200, serializer: GroupsSerializer
	end

	# match "groups/:id/molecules" => 'groups#find_molecules_of', :via => :get
	def find_molecules_of
		response.headers['Access-Control-Allow-Origin'] = '*'
		groupName = Group.find(@id).name
		isSortByMembraneSystemVal = @sort_column.start_with? 'membrane_system_id='
		
		if isSortByMembraneSystemVal
			searchMembraneSystemId = @sort_column.split('=')[-1]
			objects = Molecule.where(:group_id => @id).order("log_perm_coeff #{@sort_direction} NULLS LAST")
			.joins("LEFT JOIN experimental_measurements ON experimental_measurements.molecule_id = molecules.id AND experimental_measurements.membrane_system_id = #{searchMembraneSystemId}")
			.search(@search).paginate(:page => @page, :per_page => @per_page)
		else
			objects = Molecule.where(:group_id => @id)

			# primary order by sort column
			if Molecule.type_for_attribute(@sort_column).type == :string
				objects = objects.order("lower(#{@sort_column}) #{@sort_direction}")
			else
				objects = objects.order(@sort_column => @sort_direction)
			end

			# secondary order by id
			if @sort_column != 'id'
				objects = objects.order(:id => @sort_direction)
			end
			objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		end
		count = Molecule.where(:group_id => @id).search(@search).count
		result = paged_response(objects, count, groupName)
		render :json =>result, :status => 200, serializer: MoleculesPageSerializer
	end
	
end