module MoleculesHelper

	# returns -1 if shouldn't sort by membrane id
	# otherwise return membrane_system_id sent
	def parse_membrane_system_id(sort_column)
		isSortByMembraneSystemVal = @sort_column.start_with? 'membrane_system_id='
		return isSortByMembraneSystemVal ? @sort_column.split('=')[-1].to_i : -1
	end

end

class MoleculesController < ApplicationController
	include MoleculesHelper

	# declared in the ApplicationController
	before_action :parse_paging_params
	
	# match "molecules" => 'molecules#find_all', :via => :get
	def find_all
		response.headers['Access-Control-Allow-Origin'] = '*'
		if @fileFormat != ""
			objects = Molecule.all.order(:id)
			file = file_response(Molecule.column_names, objects)
			return send_data file, :filename => "molecules-#{Date.today}.csv", :type => @fileFormat
		end

		search_membrane_system_id = parse_membrane_system_id(@sort_column)
		if search_membrane_system_id >= 0
			objects = Molecule.order("log_perm_coeff #{@sort_direction} NULLS LAST")
			.joins("LEFT JOIN experimental_measurements ON experimental_measurements.molecule_id = molecules.id AND experimental_measurements.membrane_system_id = #{search_membrane_system_id}")
			.search(@search, @search_by)
		else
			# primary order by sort column
			if Molecule.type_for_attribute(@sort_column).type == :string
				objects = Molecule.order("lower(#{@sort_column}) #{@sort_direction}")
			else
				objects = Molecule.order(@sort_column => @sort_direction)
			end

			# secondary order by id
			if @sort_column != 'id'
				objects = objects.order(:id => @sort_direction)
			end
			objects = objects.search(@search, @search_by)
		end

		count = Molecule.search(@search, @search_by).count
		objects = objects.paginate(:page => @page, :per_page => @per_page)

		result = paged_response(objects, count, "Molecules")
		render :json =>result, :status => 200, serializer: MoleculesPageSerializer
	end

	# match "molecules/:id" => 'molecules#find_one', :via => :get
	def find_one
		response.headers['Access-Control-Allow-Origin'] = '*'
		result = Molecule.find(@id)
		render :json =>result, :status => 200, serializer: MoleculesSerializer,
		include: [
			"group",
			"class_type",
			"experimental_measurements",
			"membrane_headers"
		]
	end

	# match "molecules/:id/experimental_measurements" => 'molecules#find_experimental_measurements_of', :via => :get
	def find_experimental_measurements_of
		response.headers['Access-Control-Allow-Origin'] = '*'
		moleculeName = Molecule.find(@id).name

		objects = ExperimentalMeasurement.where(:molecule_id => @id)

		# primary order by sort column
		if ExperimentalMeasurement.type_for_attribute(@sort_column).type == :string
			objects = objects.order("lower(#{@sort_column}) #{@sort_direction}")
		else
			objects = objects.order(@sort_column => @sort_direction)
		end

		# secondary order by id
		if @sort_column != 'id'
			objects = objects.order(:id => @sort_direction)
		end

		objects = objects.search(@search).paginate(:page => @page, :per_page => @per_page)
		count = ExperimentalMeasurement.where(:molecule_id => @id).search(@search).count

		result = paged_response(objects, count, moleculeName)
		render :json =>result, :status => 200, serializer: ExperimentalMeasurementsPageSerializer
	end
	
end