class ApplicationController < ActionController::API
	require 'csv'
	require 'ostruct'
	include ActiveRecord::Sanitization
	include ActiveRecord::Sanitization::ClassMethods

	def parse_paging_params_unsafe_strings
		# direction
		@sort_direction = "ASC"
		possible_sort_directions = ["ASC", "DESC"]
		if params[:direction] && possible_sort_directions.include?(params[:direction])
			@sort_direction = params[:direction]
		end
		@sort_direction = sanitize_sql_for_order(@sort_direction)

		@sort_column = "id"
		# sort
		if params[:sort] && params[:sort].to_s.length > 0
			@sort_column = params[:sort].to_s
		end
		@sort_column = sanitize_sql_for_order(@sort_column)

		# response format
		@fileFormat = ""
		possible_formats = ["csv"]
		if params[:fileFormat] && possible_formats.include?(params[:fileFormat].downcase)
			@fileFormat = params[:fileFormat].downcase
		end

	end

	def parse_paging_params
		parse_paging_params_unsafe_strings()
		
		@search = ""
		if params[:search]
			@search = params[:search]
		end

		@search_by = "full"
		if params[:search_by]
			@search_by = params[:search_by]
		end

		#pageSize
		@per_page = 50
		if params[:pageSize] && params[:pageSize].to_i > 0
			@per_page = params[:pageSize].to_i
		end
		
		#pageNum
		@page = 1
		if params[:pageNum] && params[:pageNum].to_i > 0
			@page = params[:pageNum].to_i
		end

		#id
		@id = 0
		if params[:id] && params[:id].to_i > 0
			@id = params[:id].to_i
		end

	end

	def file_response(headers, objects)
		CSV.generate(:headers => true) do |csv|
			csv << headers
			objects.each do |obj|
				csv << headers.map{ |col|
					case when col == "pdbid"
						then "=\"#{obj[col]}\""
					else
						obj[col]
					end
				}
			end
		end
	end

	def paged_response(objects, total_objects, name)
		page_start = @per_page * (@page-1) + 1
		page_end = page_start + objects.length - 1
		if objects.length == 0
			page_start = 0
			page_end = 0
		end

		result = OpenStruct.new(
			:name => name,
			:page_num => @page,
		 	:direction => "#{@sort_direction}",
		 	:sort => "#{@sort_column}",
		 	:total_objects => total_objects,
		 	:page_size => @per_page, 
		 	:page_start => page_start, 
		 	:page_end => page_end,
		 	:objects => objects)
		return result
	end

end
