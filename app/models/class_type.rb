class ClassType < ApplicationRecord
	has_many :molecules, dependent: :destroy
end
