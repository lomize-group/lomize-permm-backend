class Group < ApplicationRecord
	has_many :molecules, dependent: :destroy
end
