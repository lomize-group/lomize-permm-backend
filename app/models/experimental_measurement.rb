class ExperimentalMeasurement < ApplicationRecord
	belongs_to :molecule, counter_cache: true
	belongs_to :group
	belongs_to :class_type
	belongs_to :membrane_system, counter_cache: true

	def update_caches
		# update caches
		self.molecule_name_cache = self.molecule.name

		# update calculation cache if there is a need for it.
		# different membrane systems need different calculations cached.
		self.molecule_logp0_cache = nil
		self.molecule_logp_cache = nil
		if !self.membrane_system.logp0_calculation_name.nil?
			self.molecule_logp0_cache = self.molecule[self.membrane_system.logp0_calculation_name]
		end
		if !self.membrane_system.logp_calculation_name.nil?
			self.molecule_logp_cache = self.molecule[self.membrane_system.logp_calculation_name]
		end
		
		self.group = self.molecule.group
		self.group_name_cache = self.group.name
		self.class_type = self.molecule.class_type
		self.class_type_name_cache = self.class_type.name
	end

	include PgSearch::Model
  	pg_search_scope :search_full_text, 
  		:against => [:reference],
  		:associated_against => {
		    :molecule => :molecule_code
		},
	  	using: {
	      :tsearch => {:prefix => true}
	    }
end
