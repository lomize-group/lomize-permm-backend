class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  def self.search(query, search_by="full")
    if query.present?
      if search_by == "name"
        search_name(query)
      else
        search_full_text(query)
      end
    else
      # No query? Return all records, no particular order.
      order("")
    end
  end
end
