class Molecule < ApplicationRecord
	has_many :experimental_measurements, dependent: :destroy
	belongs_to :class_type, counter_cache: true
	belongs_to :group, counter_cache: true

	def update_caches
		# update caches
		self.group_name_cache = self.group.name
		self.class_type_name_cache = self.class_type.name
		# self.has_ionized_charged_state_cache = self.experimental_measurements.exists?(:charged_state => "ionized")
	end

	include PgSearch::Model

	pg_search_scope :search_name,
		:against => [:molecule_code],
		:using => {
			:tsearch => {
				:prefix => true
			}
		}

  	pg_search_scope :search_full_text, 
  		:against => [
  			:molecule_code, :name, :wiki_name, :influx, :efflux, 
  			:intracellular_localization, :annotation, :pubchem
  		],
	  	:using => {
			:tsearch => {
				:prefix => true,
				:only => [
					:molecule_code, :name, :wiki_name, :influx, :efflux, 
					:intracellular_localization, :annotation
				]
			}
	    }
end
