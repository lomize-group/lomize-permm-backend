include ActiveModel::Serialization
class MembraneSystemsPageSerializer < ActiveModel::Serializer
  type :membrane_systems
  attributes :direction, :sort, :total_objects, :name,
   :page_size, :page_start, :page_end, :page_num,
   :objects

  has_many :objects, serializer: MembraneSystemsSerializer
    
end
