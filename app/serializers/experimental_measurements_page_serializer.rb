include ActiveModel::Serialization
class ExperimentalMeasurementsPageSerializer < ActiveModel::Serializer
  type :experimental_measurements
  attributes :direction, :sort, :total_objects, :name,
   :page_size, :page_start, :page_end, :page_num,
   :objects

  has_many :objects, serializer: ExperimentalMeasurementsSerializer
    
end
