include ActiveModel::Serialization
class MoleculesSerializer < ActiveModel::Serializer
	attributes :id, :name, :molecule_code, :membrane_binding_energy, :logp7_4_blm, 
  				:logp0_blm, :logp0_caco, :logp0_bbb, :logp6_5_pampa_ds, :logp7_4_pampa_ds,
          :logp0_pampa_ds, :logp6_5_caco, :logp0_caco, :logp0_bbb, :logp0_pm, :molecular_weight,
  				:pKa, :wiki_name, :pubchem, :metabolome, :chEBI, :drugbank, :KEGG, :PDB, :influx, :efflux, 
  				:intracellular_localization, :annotation, :membrane_systems,
          :group_id, :group_name_cache, :class_type_id, :class_type_name_cache

    has_one :group, serializer: GroupsSerializer
    has_one :class_type, serializer: ClasstypesSerializer

    has_many :experimental_measurements
    has_many :membrane_headers, serializer: MembraneSystemsSerializer

    def group
      object.group
    end

    def class_type
      object.class_type
    end

    def experimental_measurements
      measurements = {}
      object.experimental_measurements.each do |measurement|
        obj = {}
        obj['pubmed'] = measurement.pubmed
        obj['charged_state'] = measurement.charged_state
        obj['log_perm_coeff'] = measurement.log_perm_coeff

        if measurements.key?(measurement.membrane_system_id)
          measurements[measurement.membrane_system_id].push(obj)
        else
          measurements[measurement.membrane_system_id] = [obj]
        end
      end

      return measurements
    end

    def membrane_headers
      MembraneSystem.all.order(:id)
    end

  	def membrane_systems
      systems_struct = {}
      systems = MembraneSystem.all.order(:id)

      systems.each do |system|
        systems_struct[system.id] = nil
      end
      object.experimental_measurements.each do |measurement|
        systems_struct[measurement.membrane_system_id] = {
          :log_perm_coeff => measurement.log_perm_coeff,
          :charged_state => measurement.charged_state
        }
      end      

      return systems_struct
    end
end
