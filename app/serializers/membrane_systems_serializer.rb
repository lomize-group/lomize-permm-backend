include ActiveModel::Serialization
class MembraneSystemsSerializer < ActiveModel::Serializer
  attributes :id, :name, :experimental_measurements_count
end