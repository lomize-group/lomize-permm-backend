include ActiveModel::Serialization
class ClasstypesSerializer < ActiveModel::Serializer
  attributes :id, :name, :molecules_count
end