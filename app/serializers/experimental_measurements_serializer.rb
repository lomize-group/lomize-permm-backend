include ActiveModel::Serialization
class ExperimentalMeasurementsSerializer < ActiveModel::Serializer
	attributes :id, :log_perm_coeff, :charged_state, :reference, :pubmed,
			:molecule_id, :molecule_name_cache, :molecule_logp0_cache, :molecule_logp_cache,
			:group_id, :group_name_cache, :class_type_id, :class_type_name_cache

  	has_one :molecule, serializer: MoleculesSerializer
  	has_one :membrane_system, serializer: MembraneSystemsSerializer

  	def molecule
		object.molecule
	end

	def membrane_system
		object.membrane_system
	end
end