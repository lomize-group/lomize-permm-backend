include ActiveModel::Serialization
class GroupsSerializer < ActiveModel::Serializer
  attributes :id, :name, :letter_code, :molecules_count
end