include ActiveModel::Serialization
class GroupsPageSerializer < ActiveModel::Serializer
  type :groups
  attributes :direction, :sort, :total_objects, :name,
   :page_size, :page_start, :page_end, :page_num,
   :objects

  has_many :objects, serializer: GroupsSerializer
    
end
