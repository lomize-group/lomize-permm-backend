include ActiveModel::Serialization
class MoleculesPageSerializer < ActiveModel::Serializer
  type :molecules
  attributes :direction, :sort, :total_objects, :name,
   :page_size, :page_start, :page_end, :page_num, :membrane_headers,
   :objects

  has_many :objects, serializer: MoleculesSerializer

  def membrane_headers
    membrane_arr = []
    
    membraneObjects = MembraneSystem.all.order('id ASC')

    membraneObjects.each do |membrane|
      newEntry = {}
      newEntry['id'] = membrane.id
      newEntry['name'] = membrane.name
      membrane_arr.push(newEntry)
    end 

    return membrane_arr

  end
    
end
