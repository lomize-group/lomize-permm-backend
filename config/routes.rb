Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # these routes are explicitly defined for ease of reading
  # this should really be nested if routes beyond simple gets are implemented
  # nesting should never be more than one level deep

  match "molecules" => 'molecules#find_all', :via => :get
  match "molecules/:id" => 'molecules#find_one', :via => :get
  match "molecules/:id/experimental_measurements" => 'molecules#find_experimental_measurements_of', :via => :get

  match "groups" => 'groups#find_all', :via => :get
  match "groups/:id" => 'groups#find_one', :via => :get
  match "groups/:id/molecules" => 'groups#find_molecules_of', :via => :get

  match "classtypes" => 'classtypes#find_all', :via => :get
  match "classtypes/:id" => 'classtypes#find_one', :via => :get
  match "classtypes/:id/molecules" => 'classtypes#find_molecules_of', :via => :get

  match "membrane_systems" => 'membrane_systems#find_all', :via => :get
  match "membrane_systems/:id" => 'membrane_systems#find_one', :via => :get
  match "membrane_systems/:id/experimental_measurements" => 'membrane_systems#find_experimental_measurements_of', :via => :get

  match "experimental_measurements" => 'experimental_measurements#find_all', :via => :get
  match "experimental_measurements/:id" => 'experimental_measurements#find_one', :via => :get

  ## Other one offs
  match "stats" => 'stats#basic_stats', :via => :get

end
