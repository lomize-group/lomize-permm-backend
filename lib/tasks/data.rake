namespace :data do

	desc "pulls the production data from heroku"
	task :pull do
	  app = 'lomize-group-permm'
	  remote_db = 'DATABASE_URL'
	  local_dev_db = 'lomize-permm_development'

	  drop_db = 'rake db:drop'
	  set_env = 'rake db:environment:set'
	  pull_db = 'heroku pg:pull '+remote_db+' '+local_dev_db+' --app '+app
	  clone_test_db = 'rake db:test:prepare'
	  
	  cmd = drop_db+' && '+pull_db+' && '+set_env+' && '+clone_test_db
	  puts cmd
	  system cmd
	end

	desc "saves a dump of production database on heroku and locally"
	task :backup_production do
		puts "============================================"
		puts "============================================"
		puts "BACKING UP & DOWNLOADING PRODUCTION DATABASE"
		puts "(saving as lastest.dump)"
		puts "============================================"
		puts "============================================"

		app = 'lomize-group-permm'
		heroku = '/usr/local/bin/heroku'

		# save backup postgres dump locally just in case
		# this is also saved for a limited time in heroku
		cmd = 'rm latest.dump'
		system cmd
		cmd = heroku+' pg:backups:capture --app '+app
		system cmd
		cmd = heroku+' pg:backups:download'
		system cmd
	end

	# desc "rebuilds the data from migrations"
	# task :rebuild do
		
	# 	drop_db = "rake db:drop"
	# 	create_db = "rake db:create"
	# 	migrate_db = "rake db:migrate"
	# 	seed_db = "rake db:seed"
	# 	update_cache = "rake data:update_caches"

	# 	cmd = drop_db+' && '+create_db+' && '+migrate_db +' && '+seed_db +' && '+update_cache
	# 	puts cmd
	# 	system cmd
	# end

	desc "Remakes cached data"
	task update_caches: :environment do
		count = Molecule.all.count
		puts "Updating Cache for #{count} Molecules" 
		Molecule.find_each(:batch_size => 1000) do |object|
		  object.update_caches
		  object.save
		end
		puts :json=> Molecule.first

		count = ExperimentalMeasurement.all.count
		puts "Updating Cache for #{count} ExperimentalMeasurements" 
		ExperimentalMeasurement.find_each(:batch_size => 1000) do |object|
		  object.update_caches
		  object.save
		end
		puts :json=> ExperimentalMeasurement.first

	end

end
