namespace :data do
	namespace :import do
		task permeability_file: :environment do
			require 'csv'

			filename = "permeability.csv"
			input_path = File.join Rails.root, "db", filename
			permeability_file = File.open input_path, "r"

			CSV.foreach(permeability_file, :headers => true) do |row|
				mol_name = row[0]
				group = row["Group"]

				logp7_blm = row["logP7.4calc BLM"]
				logp0_blm = row["logPocalc BLM"]

				logp0_pampa_ds = row["logPo calc PAMPA-DS"]
				logp6_pampa_ds = row["logP6.5 calc PAMPA-DS"]
				logp7_pampa_ds = row["logP7.4 calc PAMPA-DS"]

				logp0_caco = row["logPo calc Caco2/MDCK"]
				logp6_caco = row["logP6.5 calc Caco2/MDCK"]

				logp0_bbb = row["logPo calc BBB"]
				logp0_pm = row["logPocalc PM"]


				# membrane systems cache different types of measurements.
				# avoids redundant and unused columns.
				MembraneSystem.find_each(:batch_size => 1000) do |ms|
					ms.logp0_calculation_name = nil
					ms.logp_calculation_name = nil
					if ms.id == 1
						ms.logp0_calculation_name = "logp0_blm"
						ms.logp_calculation_name = "logp7_4_blm"
					elsif ms.id == 2
						ms.logp0_calculation_name = "logp0_pampa_ds"
					elsif ms.id == 3
						ms.logp0_calculation_name = "logp6_5_pampa_ds"
					elsif ms.id == 4
						ms.logp0_calculation_name = "logp7_4_pampa_ds"
					elsif ms.id == 8
						ms.logp0_calculation_name = "logp0_caco"
					elsif ms.id == 9
						ms.logp0_calculation_name = "logp6_5_caco"
					elsif ms.id == 10
						ms.logp0_calculation_name = "logp0_bbb"
					end
					ms.save!
				end

				molecule = Molecule.find_by(:name => mol_name)
				if molecule.nil?
					puts mol_name+","+logp0_pampa_ds
				else
					molecule.update_attributes(
						:logp7_4_blm => logp7_blm,
						:logp0_blm => logp0_blm,
						:logp6_5_pampa_ds => logp6_pampa_ds,
						:logp7_4_pampa_ds => logp7_pampa_ds,
						:logp0_pampa_ds => logp0_pampa_ds,
						:logp0_caco => logp0_caco,
						:logp6_5_caco => logp6_caco,
						:logp0_bbb => logp0_bbb,
						:logp0_pm => logp0_pm
					)
					molecule.save!
				end
			end

			permeability_file.close
		end
	end
end