namespace :deploy do
  
  #invoke: rake deploy:from_scratch
  desc "This task deploys the website to heroku from from_scratch run [$ rake deploy:from_scratch] to deploy this brand new!"
  task :code do
  	app = "lomize-group-permm"
  	heroku = "/usr/local/bin/heroku"
    cmd = "heroku git:remote -a " + app
    system cmd
  	cmd = "git push heroku master"
    system cmd
  end

  desc "Backs up & deploys the app including your local database with its data. Usually run [$ rake data:pull] at some point before this."
  task :from_local do
    puts "======================================="
    puts "======================================="
    puts "WARNING: COPYING LOCAL DB TO PRODUCTION"
    puts "======================================="
    puts "======================================="

    app = 'lomize-group-permm'
    heroku = '/usr/local/bin/heroku'
    local_db = 'lomize-permm_development'
    remote_db = 'DATABASE_URL'

    cmd="rake data:backup_production"
    puts cmd
    system cmd

    reset_db = heroku+' pg:reset --app '+app+' --confirm '+app
    push_db = heroku+' pg:push '+local_db+' '+remote_db+' --app '+app
    set_env = heroku+' run rake db:environment:set'

    cmd = 'git push heroku master && '+reset_db+' && '+push_db+' && '+set_env
    puts cmd
    system cmd
  end

  # desc "This task deploys the website to heroku from from_scratch run [$ rake deploy:from_scratch] to deploy this brand new!"
  # task :from_scratch do
  #   app = "lomize-group-permm"
  #   heroku = "/usr/local/bin/heroku"

  #   reset_db = heroku+' pg:reset --app '+app+' --confirm '+app+'';
  #   migrate_db = heroku+' run rake db:migrate --app '+app
  #   seed_db = heroku+' run rake db:seed --app '+app
  #   redo_cache = heroku+ " run rake data:update_caches"
  #   #set figaro variables
  #   # set_config_vars = 'figaro heroku:set -e production --app ' + app
  #   cmd = "heroku git:remote -a " + app
  #   system cmd

  #   cmd = "git push heroku master && " + reset_db+' && '+migrate_db+' && '+seed_db +' && '+redo_cache 
  #   puts cmd
  #   system cmd
  # end

  desc "This task tests the website in prod mode on local [$ rake deploy:test_prod]"
  task :test_prod do

    cmd = 'bin/rails server -p 13314 -e production'
    puts cmd
    system cmd

  end

end