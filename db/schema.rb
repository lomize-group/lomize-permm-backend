# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190321002205) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "fuzzystrmatch"
  enable_extension "pg_trgm"

  create_table "class_types", force: :cascade do |t|
    t.string   "name"
    t.string   "comment"
    t.integer  "molecules_count", default: 0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "experimental_measurements", force: :cascade do |t|
    t.integer  "molecule_id"
    t.string   "molecule_name_cache"
    t.float    "molecule_logp0_cache"
    t.integer  "group_id"
    t.string   "group_name_cache"
    t.integer  "class_type_id"
    t.string   "class_type_name_cache"
    t.integer  "membrane_system_id"
    t.float    "log_perm_coeff"
    t.string   "lipid_composition"
    t.string   "charged_state"
    t.float    "pH"
    t.string   "reference"
    t.integer  "pubmed"
    t.string   "comment"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
    t.float    "molecule_logp_cache"
    t.index ["class_type_id"], name: "index_experimental_measurements_on_class_type_id", using: :btree
    t.index ["group_id"], name: "index_experimental_measurements_on_group_id", using: :btree
    t.index ["membrane_system_id"], name: "index_experimental_measurements_on_membrane_system_id", using: :btree
    t.index ["molecule_id"], name: "index_experimental_measurements_on_molecule_id", using: :btree
  end

  create_table "groups", force: :cascade do |t|
    t.string   "letter_code"
    t.string   "name"
    t.string   "comment"
    t.integer  "molecules_count", default: 0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "membrane_systems", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.integer  "experimental_measurements_count", default: 0
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "logp0_calculation_name"
    t.string   "logp_calculation_name"
  end

  create_table "molecules", force: :cascade do |t|
    t.string   "name"
    t.string   "molecule_code"
    t.integer  "group_id"
    t.string   "group_name_cache"
    t.integer  "class_type_id"
    t.string   "class_type_name_cache"
    t.integer  "experimental_measurements_count", default: 0
    t.float    "membrane_binding_energy"
    t.float    "logp7_4_blm"
    t.float    "logp0_blm"
    t.integer  "molecular_weight"
    t.float    "pKa"
    t.string   "wiki_name"
    t.integer  "pubchem"
    t.string   "metabolome"
    t.integer  "chEBI"
    t.string   "drugbank"
    t.string   "KEGG"
    t.string   "PDB"
    t.string   "influx"
    t.string   "efflux"
    t.string   "intracellular_localization"
    t.string   "annotation"
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.float    "logp6_5_pampa_ds"
    t.float    "logp7_4_pampa_ds"
    t.float    "logp0_pampa_ds"
    t.float    "logp6_5_caco"
    t.float    "logp0_caco"
    t.float    "logp0_bbb"
    t.float    "logp0_pm"
    t.index ["class_type_id"], name: "index_molecules_on_class_type_id", using: :btree
    t.index ["group_id"], name: "index_molecules_on_group_id", using: :btree
  end

  add_foreign_key "experimental_measurements", "class_types"
  add_foreign_key "experimental_measurements", "groups"
  add_foreign_key "experimental_measurements", "membrane_systems"
  add_foreign_key "experimental_measurements", "molecules"
  add_foreign_key "molecules", "class_types"
  add_foreign_key "molecules", "groups"
end
