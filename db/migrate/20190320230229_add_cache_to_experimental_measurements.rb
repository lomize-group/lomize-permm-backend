class AddCacheToExperimentalMeasurements < ActiveRecord::Migration[5.0]
  def change
  	change_table :experimental_measurements do |t|
  		t.rename :molecule_log_permeability_coefficient_cache, :molecule_logp0_cache
  		t.float :molecule_logp_cache
  	end
  end
end
