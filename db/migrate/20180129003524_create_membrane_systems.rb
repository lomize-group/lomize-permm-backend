class CreateMembraneSystems < ActiveRecord::Migration[5.0]
  def change
    create_table :membrane_systems do |t|
    	t.string :name
    	t.string :description
    	t.integer :experimental_measurements_count, default: 0
      	t.timestamps
    end
  end
end
