class CreateExperimentalMeasurements < ActiveRecord::Migration[5.0]
  def change
    create_table :experimental_measurements do |t|
    	t.references :molecule, foreign_key: true, index: true
      t.string :molecule_name_cache
      t.float :molecule_log_permeability_coefficient_cache
      t.references :group, foreign_key: true, index: true
      t.string :group_name_cache
      t.references :class_type, foreign_key: true, index: true
      t.string :class_type_name_cache

    	t.references :membrane_system, foreign_key: true, index: true
    	t.float :log_perm_coeff
    	t.string :lipid_composition
    	t.string :charged_state
    	t.float :pH
    	t.string :reference
    	t.integer :pubmed
    	t.string :comment
      	t.timestamps
    end
  end
end
