class CreateGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :groups do |t|
    	t.string :letter_code
    	t.string :name
    	t.string :comment
    	t.integer :molecules_count, default: 0
      	t.timestamps
    end
  end
end
