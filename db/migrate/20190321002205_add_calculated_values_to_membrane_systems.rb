class AddCalculatedValuesToMembraneSystems < ActiveRecord::Migration[5.0]
  def change
  	change_table :membrane_systems do |t|
  		t.string :logp0_calculation_name
  		t.string :logp_calculation_name
  	end
  end
end
