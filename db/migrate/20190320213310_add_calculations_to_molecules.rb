class AddCalculationsToMolecules < ActiveRecord::Migration[5.0]
  def change
  	change_table :molecules do |t|
	  # rename columns to disambiguate
      t.rename :log_permeability_coefficient, :logp7_4_blm
      t.rename :log_permeability_coefficient_neutral, :logp0_blm

      # add columns
      t.float :logp6_5_pampa_ds
      t.float :logp7_4_pampa_ds
      t.float :logp0_pampa_ds
      t.float :logp6_5_caco
      t.float :logp0_caco
      t.float :logp0_bbb
      t.float :logp0_pm
    end

    # replaced by logp0_caco and logp0_bbb
    remove_column :molecules, :passive_permeability
  end
end
