class CreateMolecules < ActiveRecord::Migration[5.0]
  def change
    create_table :molecules do |t|
    	t.string :name
    	t.string :molecule_code
    	
        t.references :group, foreign_key: true, index: true
        t.string :group_name_cache
    	t.references :class_type, foreign_key: true, index: true
        t.string :class_type_name_cache
    	
        t.integer :experimental_measurements_count, default: 0
        t.float :membrane_binding_energy
    	t.float :log_permeability_coefficient
    	t.float :log_permeability_coefficient_neutral
    	t.string :passive_permeability
    	t.integer :molecular_weight
    	t.float :pKa
    	t.string :wiki_name
    	t.integer :pubchem
    	t.string :metabolome
    	t.integer :chEBI
    	t.string :drugbank
    	t.string :KEGG
    	t.string :PDB
    	t.string :influx
    	t.string :efflux
    	t.string :intracellular_localization
    	t.string :annotation

      	t.timestamps
    end
  end
end
