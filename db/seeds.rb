# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

#Reset all Type Objects


# Type.delete_all
# Type.delete_all

require 'json'

#This assumes an import from a Json export
filename = "permm.json"
filepath = File.join Rails.root, "db", filename

old_tables = {}
current_table = ""
File.open(filepath).each do |line|
	if line[0..1] == "//"
		current_table = line[3...-1]
	elsif line[0] == "["
		old_tables[current_table] = JSON.parse(line)
		puts "Reading old table: " + current_table
	end
end

new_tables = {}
puts "Removing old tables and re-building with seed....\n\n"
Group.destroy_all
MembraneSystem.destroy_all
ClassType.destroy_all
Molecule.destroy_all
ExperimentalMeasurement.destroy_all

## ==================== creating new ClassType ====================
## ==================== creating new ClassType ====================
old_db_table = "permm.Classes"
new_type = "ClassType"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

# puts "Test print:"
# puts old_tables

for object in old_tables[old_db_table] do	
	id = object["class_id"]
	new_obj = ClassType.create(
		name: object["name"],
		comment: object["comment"] 
		)
	new_tables[new_type][id] = new_obj
end
puts :json=> ClassType.first
puts "Creating " +ClassType.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new Group ====================
## ==================== creating new Group ====================
old_db_table = "permm.Groups"
new_type = "Group"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	id = object["group_id"]
	new_obj = Group.create(
		letter_code: object["letter_code"],
		name: object["name"],
		comment: object["comment"]
		)

	new_tables[new_type][id] = new_obj
end
puts :json=> Group.first
puts "Creating " +Group.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new MembraneSystem ====================
## ==================== creating new MembraneSystem ====================
old_db_table = "permm.Systems"
new_type = "MembraneSystem"
puts "Migrating " + old_db_table
new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	id = object["system_id"]
	new_obj = MembraneSystem.create(
		name: object["name"],
		description: object["comment"],
		)

	new_tables[new_type][id] = new_obj

end
puts :json=> MembraneSystem.first
puts "Creating " +MembraneSystem.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new Molecule ====================
## ==================== creating new Molecule ====================

old_db_table = "permm.Molecules"
new_type = "Molecule"
puts "Migrating " + old_db_table
new_tables[new_type] = {}


for object in old_tables[old_db_table] do
	id = object["molecule_id"]
	new_obj = Molecule.create(
		name: object["name"],
		molecule_code: object["molid"], 
		group: new_tables["Group"][object["group_id"]],
		class_type: new_tables["ClassType"][object["class_id"]],
		membrane_binding_energy: object["membrane_binding_energy"],
		log_permeability_coefficient: object["log_permeability_coefficient"],
		log_permeability_coefficient_neutral: object["log_permeability_coefficient_neutral"],
		passive_permeability: object["passive_permeability"],
		molecular_weight: object["molecular_weight"],
		pKa: object["pKa"],
		wiki_name: object["wiki_name"],
		pubchem: object["pubchem"],
		metabolome: object["metabolome"],
		chEBI: object["chEBI"],
		drugbank: object["drugbank"],
		KEGG: object["KEGG"],
		PDB: object["PDB"],
		influx: object["influx"],
		efflux: object["efflux"],
		intracellular_localization: object["intracellular_localization"],
		annotation: object["annotation"]
		)

	new_tables[new_type][id] = new_obj

end
puts :json=> Molecule.first
puts "Creating " +Molecule.all.count.to_s+ " of type '"+new_type+"'\n\n"

## ==================== creating new ExperimentalMeasurement ====================
## ==================== creating new ExperimentalMeasurement ====================

old_db_table = "permm.Permeability"
new_type = "ExperimentalMeasurement"
puts "Migrating " + old_db_table

new_tables[new_type] = {}

for object in old_tables[old_db_table] do
	if object["compound_id"].strip == ''
		next
	end
	id = object["perm_id"]
	mol = new_tables["Molecule"][object["compound_id"].strip]
	new_obj = ExperimentalMeasurement.create(
		class_type: mol.class_type,
		group: mol.group,
		molecule: mol,
		membrane_system: new_tables["MembraneSystem"][object["system_id"]],
		log_perm_coeff: object["log_perm_coeff"],
		lipid_composition: object["lipid_composition"],
		charged_state: object["data_type"],
		pH: object["pH"],
		reference: object["reference"],
		pubmed: object["pubmed"],
		comment: object["comment"]
		)

	new_tables[new_type][id] = new_obj
end
puts :json=> ExperimentalMeasurement.first
puts "Creating " +ExperimentalMeasurement.all.count.to_s+ " of type '"+new_type+"'\n\n"