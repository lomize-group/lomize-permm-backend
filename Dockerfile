FROM ruby:2.7.2-alpine as base
RUN apk update \
    && apk add --update \
        postgresql-dev
CMD sh

FROM base as deps
WORKDIR /app
RUN apk update \
    && apk add --update \
        build-base
COPY Gemfile Gemfile.lock .
RUN bundle install --jobs=3

FROM base
COPY --from=deps /usr/local/bundle /usr/local/bundle
RUN adduser -D user \
    && apk add --update \
        curl \
        npm \
        postgresql
USER user
WORKDIR /home/user/app
COPY --chown=user:user . .